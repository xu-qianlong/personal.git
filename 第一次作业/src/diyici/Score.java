package diyici;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Scanner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Score {
 public static void main(String[] args) throws Exception {
  // 读取两个html文件
  File f1 = new File("small.html");
  File f2 = new File("all.html");
  // 读取配置文件
  Properties prop = new Properties();
  prop.load(new FileInputStream("total.properties"));
  Enumeration fileName = prop.propertyNames();
  double Score_Before = Integer.parseInt(prop.getProperty("before"));
  double Score_Base = Integer.parseInt(prop.getProperty("base"));
  double Score_Test = Integer.parseInt(prop.getProperty("test"));
  double Score_Program = Integer.parseInt(prop.getProperty("program"));
  double Score_Add = Integer.parseInt(prop.getProperty("add"));
  
  // 截取文档中的经验值并换算输出
  grab(f1, f2, Score_Before, Score_Base, Score_Test, Score_Program,Score_Add);
 
 }

 private static void grab(File small_File, File all_File, double Score_Before, double Score_Base,double Score_Test, double Score_Program, double Score_Add) {
  // 初始化定义五种经验
  int my_Before = 0;
  int my_Base = 0;
  int my_Test = 0;
  int my_Program = 0;
  int my_Add = 0;

  try {
   org.jsoup.nodes.Document document = Jsoup.parse(small_File, "UTF-8");
   org.jsoup.nodes.Document document2 = Jsoup.parse(all_File, "UTF-8");
   // 对小班课上的五种经验值统分
   if (document != null) {
    // 将所有含有目标信息目标区域找出来
    Elements es = document.getElementsByAttributeValue("class", "interaction-row");
    int temp;
    // 开始遍历各个目标区域并筛选数据
    for (int i = 0; i < es.size(); i++) {
     Element rows_ChildElement = document.select("div[class=interaction-row]").get(i);
     //添加两个判断条件，筛选出想要的数据
     if (rows_ChildElement.child(1).child(0).select("span").get(1).toString().contains("课堂完成")) {
      if (rows_ChildElement.child(1).child(2).child(0).select("span").toString().contains("已参与")) {
       Scanner sc = new Scanner(rows_ChildElement.child(1).child(2).child(0).select("span").get(7).text());
       temp = sc.nextInt();
       my_Base = my_Base + temp;
      }
     } else if (rows_ChildElement.child(1).child(0).select("span").get(1).toString().contains("课堂小测")) {
      if (rows_ChildElement.child(1).child(2).child(0).select("span").toString().contains("已参与")) {
       Scanner sc = new Scanner(es.get(i).child(1).child(2).children().get(0).children().get(7).text());
       temp = sc.nextInt();
       my_Test = my_Test + temp;
      } else if (rows_ChildElement.child(1).child(0).select("span").get(1).toString().contains("编程题")) {
       if (rows_ChildElement.child(1).child(2).child(0).select("span").toString().contains("已参与")) {
        Scanner sc = new Scanner(es.get(i).child(1).child(2).children().get(0).children().get(7).text());
        temp = sc.nextInt();
        my_Program = my_Program + temp;
       }
      } else if (rows_ChildElement.child(1).child(0).select("span").get(1).toString().contains("附加题")) {
       if (rows_ChildElement.child(1).child(2).child(0).select("span").toString().contains("已参与")) {
        Scanner sc = new Scanner(es.get(i).child(1).child(2).children().get(0).children().get(7).text());
        temp = sc.nextInt();
        my_Add = my_Add + temp;
       }
      } else {
       if (es.get(i).child(1).child(0).toString().contains("课前自测"))
        if (es.get(i).child(1).child(2).toString().contains("color:#8FC31F")) {
         Scanner sc = new Scanner(es.get(i).child(1).child(2).children().get(0).children().get(10).text());
         temp = sc.nextInt();
         my_Before = my_Before + temp;
        }
      }
     }
    }
   }

   if (document2 != null) {
    Elements es2 = document2.getElementsByAttributeValue("class", "interaction-row");
    int temp2;
    for (int i = 0; i < es2.size(); i++)
     if (es2.get(i).child(1).child(0).toString().contains("课前自测")) {
      if (es2.get(i).child(1).child(2).toString().contains("color:#8FC31F")) {
       Scanner sc2 = new Scanner(
         es2.get(i).child(1).child(2).children().get(0).children().get(10).text());
       temp2 = sc2.nextInt();
       my_Before = my_Before + temp2;
      }
     }
   }
   // 计算各部分成绩
   double my_final_Before = my_Before / Score_Before * 100 * 0.25;
   double my_final_Base = my_Base / Score_Base * 100 * 0.3 * 0.95;
   double my_final_Test = my_Test / Score_Test * 100 * 0.2;
   double my_final_Program = my_Program / Score_Program * 100 * 0.1;
   double my_final_Add = my_Add / Score_Add * 100 * 0.05;
   double my_final_Score = (my_final_Before + my_final_Base + my_final_Test + my_final_Program + my_final_Add)* 0.9 + 6;
   System.out.println(String.format("%.2f", my_final_Score));
  } catch (IOException e) {
   System.out.println("解析出错！");
   e.printStackTrace();
  }
 }
}